"use strict"
const stopBtn = document.getElementById('stopBtn');
const startBtn = document.getElementById('startBtn');
const trackItems = document.querySelectorAll('.slider img');
let i = 0;

function slide() {
    if (i === trackItems.length - 1) {
        trackItems[i].style.display = "none";
        i = 0;
        trackItems[0].style.display = "block";
    } else {
        trackItems[i].style.display = "none";
        trackItems[i + 1].style.display = "block";
        i++;
    }

    let fadeOut = document.querySelector('#fade');
    fadeOut.style.opacity = '0';
    fadeOut.style.transition = `${.25}s`
    setTimeout(() => {
        fadeOut.style.opacity = '1';
    }, 500)

    let fadeIn = document.querySelector('#fade');
    fadeIn.style.opacity = '1';
    fadeIn.style.transition = `${.25}s`
    setTimeout(() => {
        fadeIn.style.opacity = '0';
    }, 2500)
}

let timer = setInterval(slideTimer => {
    slide();
    let counter = 3;
    const paragraphTimer = document.querySelector("p");
    document.body.querySelector('p').style.fontSize = '18px';
    document.body.querySelector('p').style.color = 'blue';

    const idInterval = setInterval(() => {
        counter -= 1 / 60;
        let msVal = Math.floor((counter - Math.floor(counter)) * 100);
        let secondVal = Math.floor(counter) - Math.floor(counter / 60) * 60;
        let total = secondVal + msVal;
        if (total === 0) {
            clearInterval(idInterval);
        }

        paragraphTimer.textContent = `Наступний слайд через  ${secondVal},${msVal} секунди`;
    }, 1000 / 60);
}, 3000);

stopBtn.addEventListener('click', () => clearInterval(timer));
startBtn.addEventListener('click', () => timer = setInterval(slide, 3000));

stopBtn.addEventListener('mousedown', () => stopBtn.classList.toggle('active'));
stopBtn.addEventListener('mouseup', () => stopBtn.classList.toggle('active'));

startBtn.addEventListener('mousedown', () => startBtn.classList.toggle('active'));
startBtn.addEventListener('mouseup', () => startBtn.classList.toggle('active'));


